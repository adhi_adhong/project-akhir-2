<?php

namespace App\Http\Controllers;

use App\Paket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaketController extends Controller
{

    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table Pakets
        $Pakets = Paket::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Paket',
            'data'    => $Pakets  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find Paket by ID
        $Paket = Paket::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Paket',
            'data'    => $Paket 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'noResi'   => 'required',
            'kurir' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $Paket = Paket::create([
            'noResi'     => $request->noResi,
            'kurir'   => $request->kurir
        ]);

        //success save to database
        if($Paket) {

            return response()->json([
                'success' => true,
                'message' => 'Paket Created',
                'data'    => $Paket  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Paket Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $Paket
     * @return void
     */
    public function update(Request $request, Paket $Paket)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'noResi'   => 'required',
            'kurir' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find Paket by ID
        $Paket = Paket::findOrFail($Paket->id);

        if($Paket) {

            //update Paket
            $Paket->update([
                'noResi'     => $request->noResi,
                'kurir'   => $request->kurir
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Paket Updated',
                'data'    => $Paket  
            ], 200);

        }

        //data Paket not found
        return response()->json([
            'success' => false,
            'message' => 'Paket Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Paket by ID
        $Paket = Paket::findOrfail($id);

        if($Paket) {

            //delete Paket
            $Paket->delete();

            return response()->json([
                'success' => true,
                'message' => 'Paket Deleted',
            ], 200);

        }

        //data Paket not found
        return response()->json([
            'success' => false,
            'message' => 'Paket Not Found',
        ], 404);
    }
}

